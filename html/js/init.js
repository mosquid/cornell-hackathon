module.exports = function() {
  navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

  const constraints = {
    video: true,
    video: { facingMode: "environment" }
  }
  const video = document.querySelector('video');
  const ta    = document.querySelector('textarea');
  const wave  = document.querySelector('#loader');
  const icons = document.querySelectorAll('.expand img')
  const date = document.querySelector('#date')

  navigator.getUserMedia(constraints, (stream) => {video.srcObject = stream}, function(er) {console.log(err);})

  setTimeout(function() {
    if (video.classList.contains('freeze'))
      video.pause()
  }, 1500)

  var btn = document.querySelector('#primary-btn')
  if (btn) {  
    btn.addEventListener('touchstart', function() {
      this.classList.add('press')
      wave.classList.add('show')
      video.pause()
    })
    btn.addEventListener('touchend', function(e) {
      this.classList.remove('press')
      window.location.href = window.location.href + 'edit.html'
    })
  }

  if (date) {
    date.addEventListener('change', function() {
      document.querySelector('#todate').innerText = this.value
    })
  }

  if (icons.length) {
    icons.forEach(icon => {
      icon.addEventListener('click', function() {
        document.querySelector('#holder img').src = this.src
        ta.focus()
      })
    })
  }
}